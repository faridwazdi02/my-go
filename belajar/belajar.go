package main

import "fmt"

func main() {
	// var frd string = "Farid"
	// var str1, str2 string = "Farid", "Wazdi"

	var p1 = person{}
	p1.fname = "Farid"
	p1.lname = "Farid"
	p1.age = 20
	p1.email = "Faridwazdi02@gmail.com"
	p1.password = "passwd"

	var namaPanjang string = p1.getNamaPanjang()
	fmt.Println("First Name : ", p1.fname, " Last Name : ", p1.lname)
	fmt.Printf(" Age : %d", p1.age)
	fmt.Println("\n namaPanjang : ", namaPanjang)

}

func getNamaPanjang(fname string, lname string) string {
	return (fname + " " + lname)
}
