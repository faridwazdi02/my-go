package main

type person struct {
	fname    string
	lname    string
	age      int
	email    string
	password string
}

func (p person) getNamaPanjang() string {
	return p.fname + " " + p.lname
}
